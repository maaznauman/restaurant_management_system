#ifndef cookies_h
#define cookies_h
#include <iostream>
#include "dessert.cpp"
using namespace std;

class cookies:public dessert
	{
		protected:
			double grams;
			string flavor;
		public:
			cookies();
			cookies(double g,string f,double pps,double ppg,string n,int i,double p,int q);
			void setgrams(double g);
			void setflavor(string f);
			double getgrams();
			string getflavor();
	};
	#endif
