#ifndef beverages_h
#define beverages_h
using namespace std;
#include "menu.h"

class beverages:public menu
	{
		protected:
			string serving;
			string templevel;
		public:
			beverages();
			beverages(string s,string tl,string n,int i,double p,int q);
			void setserving(string s);
			void settemplevel(string tl);
			string getserving();
			string gettemplevel();
	};
	
	#endif
