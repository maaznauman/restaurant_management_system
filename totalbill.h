#ifndef totalbill_h
#define totalbill_h
#include <iostream>
using namespace std;


class totalbill
	{
		protected:
			double taxrate;
			double amount;
			int quantity;
		public:
			totalbill();
			totalbill(double r,double a,int q);
			void setamount(double a);
			void setquantity(int q);
			double getamount();
			int getquantity();
			void settotalbill();
			double gettotalbill();
			
	};
	
	
	
	
#endif
