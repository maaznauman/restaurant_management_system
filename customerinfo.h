#ifndef customerinfo_h
#define customerinfo_h
#include <iostream>
using namespace std;


class customerinfo
	{
		protected:
			string firstname;
			string lastname;
			double phone;
		public:
			
			customerinfo();
			customerinfo(string fn,string ln,double p);
			void setfirstname(string fn);
			void setlastname(string ln);
			void setphone(double p);
			string getfirstname();
			string getlastname();
			double getphone();
			void displaycustomerinfo();
	};
	
#endif
