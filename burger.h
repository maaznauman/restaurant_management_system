#include <iostream>
#ifndef burger_h
#define burger_h
#include "frieditem.h"

class burger:public frieditem
{
	protected:
		string patties;
		string bun;
	public:
		burger();
		burger(string pa,string b,string ds,string fs,string bs,string n,int i,double p,int q);
		void setpatties(string p);
		void setbun(string b);
		string getbun();
		string getpatties();
		
};
#endif
