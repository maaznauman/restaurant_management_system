#include<iostream>
#ifndef dateandtime_h
#define dateandtime_h
using namespace std;


class dateandtime
	{
		protected:
			int day;
			int month;
			int year;
			int hour;
			int min;
			
		public:
			dateandtime();
			dateandtime(int d,int mon,int y,int h,int m);
			void sethour(int h);
			void setmin(int m);
			void setday(int d);
			void setmonth(int mon);
			void setyear(int y);
			int getday();
			int getmonth();
			int getyear();
			int gethour();
			int getmin();
			void displaydateandtime();
			
	};
#endif
