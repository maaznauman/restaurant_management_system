#include <iostream>
#ifndef carbonateddrink_h
#define carbonateddrink_h
#include "beverages.cpp"
using namespace std;

class carbonateddrink:public beverages
{
	protected:
		string drinkname;
		bool sugarfree;
	public:
	carbonateddrink();
	carbonateddrink(string na,string s,string tl,string n,int i,double p,int q);
	void setname(string n);
	void setsugarfree(bool a);
	string getname();
	bool getsugarfree();	
 }; 
 	

#endif
