#ifndef frieditem_h
#define frieditem_h
#include <iostream>
#include "menu.h"
using namespace std;


class frieditem:public menu
{
		protected:
			string dippingsauce;
			string friessauce;
			string burgersauce;
		public:
			frieditem();
			frieditem(string ds,string fs,string bs,string n,int i,double p,int q);
			void setdippingsauce(string ds);
			string getdippingsauce();
			void setfriessauce(string fs);
			string getfriessauce();
			void setburgersauce(string bs);
			string getburgersauce();
			void displaysauce();
				
		
		
		
		
	};


#endif
