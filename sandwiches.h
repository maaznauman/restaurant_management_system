#include<iostream>
using namespace std;
#ifndef sandwiches_h
#define sandwiches_h
#include "menu.h"


class sandwiches:public menu
	{
		protected:
			string filling;
			string type;
			
		public:
			sandwiches();
			sandwiches(string f,string t,string n,int i,double p,int q);
			void setfillings(string f);
			void settype(string t);
			string getfillings();
			string gettype();
			
			
			
	};

#endif
