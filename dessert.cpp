#include<iostream>
#include "dessert.h"
using namespace std;

dessert::dessert()
	{
		this->price_per_scoop=0;
		this->price_per_gram=0;
		menu();
	}
dessert::dessert(double pps,double ppg,string n,int i,double p,int q)
	{
		this->price_per_scoop=pps;
		this->price_per_gram=ppg;
		this->name=n;
		this->id=i;
		this->price=p;
		this->quantity=q;
	}
void dessert::setprice_per_scoop(double pps)
	{
		this->price_per_scoop=pps;
	}
void dessert::setprice_per_gram(double ppg)
	{
		this->price_per_gram=ppg;
	}
double dessert::getprice_per_scoop()
	{
		return price_per_scoop;
	}
double dessert::getprice_per_gram()
	{
		return price_per_gram;
	}
