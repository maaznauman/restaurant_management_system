#include <iostream>
using namespace std;
#ifndef nugget_h
#define nugget_h
#include "frieditem.cpp"

class nugget:public frieditem	
{	
	protected:
		bool chickennugget;
		bool tempuranugget;
		bool crispychickennugget;
		string shape;
		
	public:
		nugget();
		nugget(bool cn,bool tn,bool ccn,string ds,string fs,string bs,string n,int i,double p,int q);
		void setchickennugget(bool a);
		void settempuranugget(bool a);
		void setcrispychickennugget(bool a);
		bool getchickennugget();
		bool gettempuranugget();
		bool getcrispychickennugget();
};

#endif
