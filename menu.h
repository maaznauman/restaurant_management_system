#ifndef menu_h
#define menu_h
#include <iostream>
using namespace std;

class menu
	{
		protected:
			string name;
			int quantity;
			int id;
			double price;
		public:
			menu();
			menu(string n,int i,double p,int q);
			void setname(string n);
			void setid(int i);
			void setprice(double p);
			void setquantity(int q);
			int getquantity();
			string getname();
			int getid();
			double getprice();	
	};
	
	#endif
