#include<iostream>
#include"pizza.h"
using namespace std;


pizza::pizza()
	{
		this->crust="";
		this->size="";
		this->topping="";
		this->id=0;
		this->price=0;
	}
pizza::pizza(string c,string s,string t,int i,float p)
	{
		this->crust=c;
		this->size=s;
		this->topping=t;
		this->id=i;
		this->price=p;
	}
void pizza::setsize(string s)
	{
		if(s=="small"||s=="medium"||s=="large")
			{
				this->size=s;
			}
		else
			{
				cout<<"the size can only be small ,medium or large"<<endl;
			}
	}
string pizza::getsize()
	{
		return size;
	}
void pizza::setcrust(string c)
	{
		if( c=="fresh pan" ||c=="stuffed" ||c=="hand tossed")
			{
				this->crust=c;
			}
		else
			{
				cout<<"the crust you entered is not available"<<endl;
			}
	}
	string pizza::getcrust()
		{
			return crust;
		}
	void pizza::settopping(string t)
		{
			if(t=="pepperoni" ||t=="mushrooms" ||t=="onions" ||t=="extra cheese"|| t=="black olives")
				{
					this->topping=t;
				}
			else
				{
					cout<<"topping not available"<<endl;
				}
		}
	string pizza::gettopping()
		{
			return topping;
		} 
	void pizza::setid(int i)
		{
			this->id=i;
		}
	void pizza::setprice()
		{
			if(this->size=="small")
				{
					this->price=300;
				}
			else if(this->size=="medium")
				{
					this->price==500;
				}
			else
			{
				this->price=800;
			}
		}
	
