#ifndef freshjuice_h
#define freshjuice_h
#include<iostream>
#include"beverages.cpp"
using namespace std;
class freshjuice:public beverages
{
	private:
		string type;
		bool seasonal;
		public:
			freshjuice();
			freshjuice(string type,int seasonal,string s,string tl,string n,int i,double p,int q);
			void methodseasonal();
			void setType(string type);
			string getType();
			void setSeasonal(int s);
			bool getSeasonal();
			
	
};
#endif
