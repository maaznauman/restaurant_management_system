#include <iostream>
#ifndef fries_h
#define fries_h
#include "frieditem.cpp"
using namespace std;

class fries:public frieditem
	{
		protected:
			bool curly;
			bool plain;
			string serving;
			
		public:
			fries();
			fries(bool c,bool p,string s,string ds,string fs,string bs,string n,int i,double pr,int q);
			void setcurly(bool a);
			void setplain(bool a);
			bool getcurly();
			bool getplain();
			void setserving(string s);
			string getserving();
	};
	
	#endif
