#ifndef icecream_h
#define icecream_h
#include "dessert.cpp"
#include <iostream>
using namespace std;

class icecream:public desert
	{
		protected:
			int scoops;
			string toppings;
			
		public:
			icecream();
			icecream(int s,string t,double pps,double ppg,string n,int i,double p,int q);
			void setscoops(int s);
			void settoppings(string t);
			int getscoops();
			string gettopping();
				
	};
	#endif
