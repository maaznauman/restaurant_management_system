#ifndef order_h
#define order_h
#include "menu.cpp"
using namespace std;

class order:public menu
	{
		protected:
			menu obj[];
			int count;
		
		public:
			order();
			order(menu m, int c);
			void setmenu(menu m);
			void setcount(int c);
			void placeorder();
			
	};
	#endif
