#ifndef pizza_h
#define pizza_h
#include<iostream>
#include "maincourse.cpp"
using namespace std;



class pizza
	{
		protected:
			string size;
			string crust;
			string topping;
			int id;
			float price;
		public:
			pizza();
			pizza(string s,string c,string t,int id,float price);
			void setsize(string s);
			string getsize();
			void setcrust(string c);
			string getcrust();
			void settopping(string t);	
			string gettopping();
			void setid(int i);
			int getid();
			void setprice();
			float getprice();
			
	};
#endif
