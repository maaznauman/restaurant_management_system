#ifndef dessert_h
#define dessert_h
#include "menu.cpp"
#include <iostream>
using namespace std;

class dessert:public menu
	{
		protected:
			double price_per_scoop;
			double price_per_gram;
	
		public:
		dessert();
		dessert(double pps,double ppg,string n,int i,double p,int q);
		void setprice_per_scoop(double pps);
		void setprice_per_gram(double ppg);
		double getprice_per_scoop();
		double getprice_per_gram();
	};
#endif
