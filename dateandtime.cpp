#include<iostream>
#include"dateandtime.h"
using namespace std;

dateandtime::dateandtime()
	{
		this->day=0;
		this->month=0;
		this->year=0;
		this->hour=0;
		this->min=0;
	}
dateandtime::dateandtime(int d,int mon,int y,int h,int m)
	{
		this->day=d;
		this->month=mon;
		this->year=y;
		this->hour=h;
		this->min=m;
	}
void dateandtime::setday(int d)
	{
		this->day=d;
	}
void dateandtime::setmonth(int mon)
	{
		this->month=mon;
	}
void dateandtime::setyear(int y)
	{
		if(y>0)
			{
				this->year=y;
			}
		
	}
void dateandtime::sethour(int h)
	{
		if(h>0 && h<=23)
			{
				this->hour=h;
			}
		else
			{
				cout<<"invailid value entered for hours"<<endl;
			}
		
	}
void dateandtime::setmin(int m)
	{
		if(m>60)
			{
				this->min=m-60;
				this->hour++;
			}
		else if(m<0)
			{
				cout<<"mins cannot be negative"<<endl;
			}
		else
			{
				this->min=m;
			}
	}
void dateandtime::displaydateandtime()
	{
		cout<<this->day<<"/"<<this->month<<"/"<<this->year<<endl;
		cout<<this->hour<<":"<<this->min<<endl;
	}
